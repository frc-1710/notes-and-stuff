package jake.herman.give.frcqrscout;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;


public class MainActivity extends ActionBarActivity {

    QRCodeWriter writer = new QRCodeWriter();
    String appendedData;
    String defenseData;
    String teamData;
    String shootingData;
    int scoreHi;
    int scoreHiAtt;
    int scoreLow;
    boolean inDefense, inMain, inShooting;

    String[] shootingDataArray = {"0", "0", "0"};
    String[] defenseDataArray = {"0", "0","0","0","0", "0","0","0","0"};
    String[] teamDataArray = {"0", "0"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_main) {
            if(inDefense) {
                saveDefenseData();
            } else if(inShooting) {
                saveShooting();
            } else if(inMain) {
                saveMain();
            }
            setContentView(R.layout.activity_main);
            setMainText();
            inShooting = false;
            inMain = true;
            inDefense = false;
            return true;
        }

        if(id == R.id.action_defenses) {
            if(inDefense) {
                saveDefenseData();
            } else if(inShooting) {
                saveShooting();
            } else if(inMain) {
                saveMain();
            }
            setContentView(R.layout.defenses_crossed);
            setDefenseText();
            inShooting = false;
            inMain = false;
            inDefense = true;
            return true;
        }

        if(id == R.id.action_shooting) {
            if(inDefense) {
                saveDefenseData();
            } else if(inShooting) {
                saveShooting();
            } else if(inMain) {
                saveMain();
            }
            setContentView(R.layout.shooting_layout);
            setShootingText();
            inShooting = true;
            inMain = false;
            inDefense = false;
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setMainText() {
        EditText teamNumber = (EditText)findViewById(R.id.TeamNumber);
        EditText matchNum = (EditText)findViewById(R.id.match_num);
        for(int i = 0; i<2; i++){
            if(i==0) {
                teamNumber.setText(teamDataArray[i]);
            }
            if(i==1) {
                matchNum.setText(teamDataArray[i]);
            }
        }
    }

    public void switchToHere() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        //then do put extra profile stuff
    }

    public void saveDefenseData() {
        EditText portCScore = (EditText)findViewById(R.id.portc_score);
        EditText chevalScore = (EditText)findViewById(R.id.cheval_score);
        EditText rampScore = (EditText)findViewById(R.id.ramp_score);
        EditText moatScore = (EditText)findViewById(R.id.moat_score);
        EditText bridgeScore = (EditText)findViewById(R.id.bridge_score);
        EditText sallyScore = (EditText)findViewById(R.id.port_score);
        EditText rockScore = (EditText)findViewById(R.id.rockwall_score);
        EditText terrainScore = (EditText)findViewById(R.id.terrain_score);
        EditText lowbarScore = (EditText)findViewById(R.id.lowbar_score);

        defenseData = portCScore.getText().toString() + " " + chevalScore.getText().toString() + " " + rampScore.getText().toString() + " " +
                moatScore.getText().toString() + " " + bridgeScore.getText().toString() + " " + sallyScore.getText().toString() + " " +
                rockScore.getText().toString() + " " + terrainScore.getText().toString() + " " + lowbarScore.getText().toString() + " ";
        defenseDataArray[0] = portCScore.getText().toString();
        defenseDataArray[1] = chevalScore.getText().toString();
        defenseDataArray[2] = rampScore.getText().toString();
        defenseDataArray[3] = moatScore.getText().toString();
        defenseDataArray[4] = bridgeScore.getText().toString();
        defenseDataArray[5] = sallyScore.getText().toString();
        defenseDataArray[6] = rockScore.getText().toString();
        defenseDataArray[7] = terrainScore.getText().toString();
        defenseDataArray[8] = lowbarScore.getText().toString();
    }

    public void saveMain() {
        EditText teamNumber = (EditText)findViewById(R.id.TeamNumber);
        EditText matchNum = (EditText)findViewById(R.id.match_num);
        teamData = teamNumber.getText() + " " + matchNum.getText().toString() + " ";
        teamDataArray[0] = teamNumber.getText().toString();
        teamDataArray[1] = matchNum.getText().toString();
    }



    public void addHiMade(View V) {
        TextView hiMade = (TextView)findViewById(R.id.high_shots_made);
        scoreHi ++;
        hiMade.setText(String.valueOf(scoreHi));
    }

    public void subHiMade(View V) {
        TextView hiMade = (TextView)findViewById(R.id.high_shots_made);
        scoreHi -= 1;
        hiMade.setText(String.valueOf(scoreHi));
    }

    public void addHiAtt(View V) {
        TextView hiAtt = (TextView)findViewById(R.id.hi_shots_att);
        scoreHiAtt ++;
        hiAtt.setText(String.valueOf(scoreHiAtt));
    }

    public void subHiAtt(View V) {
        TextView hiAtt = (TextView)findViewById(R.id.hi_shots_att);
        scoreHiAtt -= 1;
        hiAtt.setText(String.valueOf(scoreHiAtt));
    }

    public void addLow(View V) {
        TextView lowMade = (TextView)findViewById(R.id.lo_shots_made);
        scoreLow ++;
        lowMade.setText(String.valueOf(scoreLow));
    }

    public void subLow(View V) {
        TextView lowMade = (TextView)findViewById(R.id.lo_shots_made);
        scoreLow -= 1;
        lowMade.setText(String.valueOf(scoreLow));
    }

    public void setDefenseText() {
        EditText portCScore = (EditText)findViewById(R.id.portc_score);
        EditText chevalScore = (EditText)findViewById(R.id.cheval_score);
        EditText rampScore = (EditText)findViewById(R.id.ramp_score);
        EditText moatScore = (EditText)findViewById(R.id.moat_score);
        EditText bridgeScore = (EditText)findViewById(R.id.bridge_score);
        EditText sallyScore = (EditText)findViewById(R.id.port_score);
        EditText rockScore = (EditText)findViewById(R.id.rockwall_score);
        EditText terrainScore = (EditText)findViewById(R.id.terrain_score);
        EditText lowbarScore = (EditText)findViewById(R.id.lowbar_score);
        for(int i = 0; i<9; i++){
            if(i==0) {
                portCScore.setText(defenseDataArray[i]);
            }
            if(i==1) {
                chevalScore.setText(defenseDataArray[i]);
            }
            if(i==2) {
                rampScore.setText(defenseDataArray[i]);
            }
            if(i==3) {
                moatScore.setText(defenseDataArray[i]);
            }
            if(i==4) {
                bridgeScore.setText(defenseDataArray[i]);
            }
            if(i==5) {
                sallyScore.setText(defenseDataArray[i]);
            }
            if(i==6) {
                rockScore.setText(defenseDataArray[i]);
            }
            if(i==7) {
                terrainScore.setText(defenseDataArray[i]);
            }
            if(i==8) {
                lowbarScore.setText(defenseDataArray[i]);
            }
        }
    }

    public void GenerateQR(View V) throws WriterException {
        ImageView img = (ImageView)findViewById(R.id.qr_generated);

        appendedData = teamData + defenseData + shootingData;

        String finalData = Uri.encode(appendedData, "utf-8");
        BitMatrix bm = writer.encode(finalData, BarcodeFormat.QR_CODE,150, 150);
        Bitmap ImageBitmap = Bitmap.createBitmap(150, 150, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < 150; i++) {//width
            for (int j = 0; j < 150; j++) {//height
                ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK: Color.WHITE);
            }
        }

        if (ImageBitmap != null) {
            img.setImageBitmap(ImageBitmap);
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.user_input_error),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void saveShooting() {
        TextView hiMade = (TextView)findViewById(R.id.high_shots_made);
        TextView hiAtt = (TextView)findViewById(R.id.hi_shots_att);
        TextView loMade = (TextView)findViewById(R.id.lo_shots_made);

        shootingData = hiMade.getText().toString() + " " + hiAtt.getText().toString() + " " + loMade.getText().toString() + " ";
        shootingDataArray[0] = hiMade.getText().toString();
        shootingDataArray[1] = hiAtt.getText().toString();
        shootingDataArray[2] = loMade.getText().toString();
    }

    public void setShootingText() {
        TextView hiMade = (TextView)findViewById(R.id.high_shots_made);
        TextView hiAtt = (TextView)findViewById(R.id.hi_shots_att);
        TextView loMade = (TextView)findViewById(R.id.lo_shots_made);

        for(int i = 0; i<3; i++) {
            if(i==0) {
                hiMade.setText(String.valueOf(shootingDataArray[i]));
            }

            if(i==1) {
                hiAtt.setText(String.valueOf(shootingDataArray[i]));
            }

            if(i==2) {
                loMade.setText(String.valueOf(shootingDataArray[i]));
            }
        }
    }
}
